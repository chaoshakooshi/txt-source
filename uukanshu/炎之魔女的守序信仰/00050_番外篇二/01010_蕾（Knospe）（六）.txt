视角：THE－GOD

学园祭的当天，天气顺应了天气预报和孩子们的心情……

天空万里无云，应着这份天气就连原本对学园祭没什麼兴趣的人都跟着躁动的人群进入这裡。

没错，一个接一个地进入这条一年之中被装饰得最华丽一次的大门。

【从我之中，进入悲惨之城。

从我之中，进入永恒的痛苦。

从我之中，走进永劫的人群。】

…………

“啊，小贺~~早上好！”

“早上好，小檩。”

在看到少年的一瞬间，芥檩的脸上浮现出惊喜的表情，但是在互相问候早安之後她稍稍鼓起了脸颊。

“前幾天丢下我先回家，就连今天也是！自己先走一步！！因为请我吃了甜点自助本来还想就那样原谅你的，现在可没那麼简单了哦。”

即使是生气的表情也是那麼可爱，应该说无论是哪种表情，自己都是爱着才对，贺由就是这麼想着的。

没错，只要对方是小檩的话，无论是怨言也好，哭诉也好，自己都能仔细倾听。

不过现在却必须忍耐这样的心情，忍耐着等到晚上说出口。

“抱歉呐，小檩，对了……关於之前的话真是对不起了。”

“嗯？”

“就是那个啦，我说学园祭我要帮你忙的，现在仔细想想我这边也是有一点应该做的事情啊。”

“啊~~能明白就好。所以说，就带上小涟一起吧。”

还真是不肯放弃了，也不明白她为什麼要这麼坚持。

“那好吧……如果她原意的话。”

“一定会同意的！放心吧，毕竟小涟她可是……”

“哎？”

“什麼都没有啦。”

“是这样啊……”

带着苦笑轻轻摇了摇头，自己也并不是迟钝到这个地步。

嘛，这样也好吧，也有必须要说的事情。

“那麼回头见了。”

“回头见，对了，夜祭的时候我去找你，就这样了。”

“诶？”

为了不听到拒绝的回復，他急忙逃离了这裡。

并非是怯懦，而是因为充满了勇气，所以必须暂时撤退。

…………

……

来到班级的时候，大家都已经开始工作了。

“啊，贺由！你来得正好~~”

看到了贺由，穿着吸血鬼打扮服装的损友道华倒是第一个迎了上来。

“唔，你这傢伙不嫌热吗？”

“只有今天比较热，我是比较倒霉啦……抽到这种複杂的衣服。我靠，这带廉价皮草的披风……又戳人又热啊。”

“算了，至少看上去还不算别扭。”

道华不说话的话本身就是一个轮廓如雕刻一般的帅哥，应该说带上金髮，红色美瞳之後反而让他更加美型了。

“先不说这个了，快抽你要扮的形象，然後到後面换衣服去。”

之前经过所有同学投票，贺由的班级决定举办的是COSPLAY咖啡厅，在青春期的少年少女们自然有着与年龄符合变装欲，不过单单穿自己喜欢的服装就太没有意思了。所以穿什麼衣服则是由抽签决定的。

道华从身後的凳子上取来抽签箱，贺由想也没想就直接取出了一张。

“哈？骑士！还有这种装扮？”

“唔哈哈哈，看起来是比我更热的打扮啊，对了，衣服给你~~”

“别开玩笑了！这种搞笑一般的东西让人怎麼穿，为什麼护肩护腿还是金属的？你们的预算这麼充足？”

“有人家裡是做道具的，这是借来的，小心点用哦。”

“我不穿！坚决不穿！这种东西会被人笑话的！！”

贺由一脸嫌弃地看着眼前的衣装，如果是身高很高像他一样的帅哥的话穿这种衣装倒也没什麼大不了的，但是自己身高面貌都平平，是整人遊戏吗？

“别耍性子了，有人还抽到小丑呢……可是整张脸都被涂得一塌糊涂哦，你也想那样吗？”

“唔，知道了！我换！我换就是啦！！”

在道华张牙舞爪的逗弄之中，受不了的贺由表示认输，老老实实地进入被帘子围起做出的后台。

“贺由君！？”

“哎？这是……”

谁知道就在他揭开帘子的那一刻，眼前出现的确是极其煽情的景色。

蛭涟半脱着制服，将丰满的胸部和黑色长袜包裹的大腿裸露在贺由的面前。

蛭涟因为这突如其来的状况完全不知道怎麼去反应，只能呆呆地注视眼前的少年，就连雪白的肌肤也忘记了去遮挡。

贺由也是，连拿在手中的骑士装也啪的一声掉落在地上。

两人就这样愣住在原地。

“啊，那个……这是，道华……让我来换衣服的，啊啊啊，总之非常抱歉！！”

不管出於何种解释，都自觉理亏的少年急忙转身想要离开这裡，可是却有一股小小的力量此时抓着他的衬衫下摆。

感受到这种微小却带有巨大压力的触感，贺由不禁掩面。

“不用太紧张……我也有话想和贺由君说……”

道华你丫的算计我！！先不管这个了，蛭涟到底是想什麼啊，这种情况为什麼要把我叫停下来，这可是直接犯罪的前奏曲啊。

虽然急迫地想冲出这道帘子，但是毕竟是女生先开口，自己这麼直接离开是不是不太好啊。

而蛭涟看到贺由并没有什麼动静，就乾脆放下正在替换着的衣服接着说下去了。

“贺由君一会有空吗？”

“一会？是指什麼时候？班上咖啡厅的活要做很久吧。”

“班上的同学们刚才告诉我，我们两人并安排在早班了……两个小时就可以结束，如果可以的话可以陪我逛学园祭吗？”

“倒是……没什麼不可以的。”

出於这种异样的压迫力，贺由想都没想就答应了蛭涟。

是蛭涟的话不会提什麼过分的要求吧，他仅仅是依照这样的惯性思维去思考了而已。

“诶！真的吗？”

可是看着对方这麼激动的样子，自己也没法再拒绝了。

“那……那个，我可以出去了吗？蛭涟同学……”

“抱歉，贺由君也要换衣服吧，那麼我就先出去了……”

“喂！你才是呢，衣服完全没穿好呢！！”

看着神情恍惚要走出更衣室的蛭涟，贺由羞红了脸颊全力拉着她。

…………

……

“真没想到蛭涟同学出乎意料地迷糊呢。”

“呜唔……请赶快忘掉！”

回想刚才的事情蛭涟才刚刚反应过来自己到底做了多麼大胆地事情，就连脸上都仿佛冒出蒸汽一般。

“……正如她所期望的，是拥有赛雪的冰肌，玫瑰色的美艳脸颊，似鲜血般的红唇，黑檀木色的墨黑头的女孩，他们为她取名为白雪公王~~嘛，我们这裡的公主倒是脸颊是红色的呢。”

看着身着公主裙脸红着的蛭涟，一旁手持点单的道华坏笑着说着。

对此，蛭涟狠狠地瞪了他一眼。

平时不怎麼说话的美人同班同学无处发泄的怒气就这样悉数砸在了道华的头上。

“唔……抱歉了，其实是有些话想和贺由君说一下。”

“贺由君？哈哈哈！道华你丫的居然还会这麼叫我？你是想笑死老子吗？哈哈哈！！”

“吵死了！！你过来！”

一把拉过损友，吸血鬼打扮的少年对着骑士悄声问了。

“明白我们的用意吗？”

“用意？”

“别这麼迟钝啊。早上的活就交给我们这边做吧，相对的，蛭涟那边就交给你了啊。”

说完并不等他回復，道华将贺由向蛭涟那边推了过去。

“……”

到底是谁迟钝啊，你们一个个都做得这麼明显谁还不知道啊。不过既然都这麼说了自然也该做些什麼了，贺由由衷地想着。

“蛭涟同学，之前说了想要一起逛学园祭来着，看来现在有空了。”

“可是现在才刚刚开始……班上的工作。”

“刚才貌似又有新的决定吧，那群傢伙给的优待来着。不管啦，就让我们好好去玩吧，啊~~这时候应该这麼说才对，可以请你把手给我吗？公主殿下？”

虽然对於这个相貌普通的少年来说是有些做作的姿态，但是在这一身合身的骑士装的印衬下倒也算是有模有样。

蛭涟面对着眼前露出温柔笑容的恋慕之人，也瞬间忘记了如何应对，只能乖乖地伸出了自己被纯白手套包裹着的右手，傍在骑士少年的身旁……

…………

……

“虽说是一年一度的学园祭，但是感觉也就那麼一回事啊……”

“学生们自己做得东西，当然比起社会人来说还是差得很远吧。”

“是呢，那个三明治可是连培根都切坏了啊。”

“那应该是看见了贺由君骑士装的缘故。”

“哪裡，我觉得一定是看见蛭涟的公主装的缘故才对。”

“吃着最糟糕三明治的骑士和公主呢。”

“还是被从咖啡厅被同班同学赶出来的骑士与公主哦~~”

“呼呼……”

“哈哈。”

两人坐在後－庭院的木椅上，吃着刚买来的三明治，互相调侃着对方的打扮。

由於时间还很早，除了下午轻音部的LIVE和话剧部的演出之外两人很快地将大致的项目逛了一圈，基本上就是吃吃东西。聊聊天，然後再沐浴一下众人好奇的目光就是了。

即使是变装人数很多的今天，他们的造型依然算是十分醒目，情侣就算了……还特意穿成这样出来秀吗？

这实在是让年纪尚轻的少年们相当不爽的事情，逛鬼屋的时候还特别被裡面打扮成的“丧尸”的同学追了很久。

不过并非不懂气氛的贺由在转了一圈满足了公主的好奇心之後便找了一个安静地方坐了下来。

夏日最後的微风吹拂着还没有发红的树叶以及少女檀木般的黑色长髮。

“我们离毕业还有多久？”

“一年零五个月……”

“总感觉和蛭涟同学一起做过图书管理员的记忆还历历在目呢。”

“诶？贺由君还记得那时候的事情吗？”

“在你的眼中我究竟是多麼的健忘啊……”

“呼呼，但是那之後可是完全没有再推荐新的书给我了哦。”

蛭涟轻轻掩住樱色的唇瓣笑了一下。

对此，少年却完全笑不出来。

是时候了吧……把一切都说出来，尽管是有些残酷，也有些自大。

但是这是必须要说的事情，不说的话自己会後悔的。

脑中酝酿好最合适的措辞，贺由开口了。

…………

“蛭涟同学，你喜欢我吗？”

“！！”

并没有怎麼犹豫，直接就说了出来。

这句话让少女手上吃到一半的三明治就这样掉落在地上……

在这段时间裡，自己也想了很多，大家为什麼想把自己和蛭涟凑成一对呢？

小檩她可能是出於对朋友的关心以及作为“姐姐”立场的责任感，道华则是身为友人推了一把力吧，不难想象，班上其他同学应该也是在这两人的请求下帮忙的。

但是蛭涟她自己又是怎麼想的呢？不问清楚本人的想法……不行。

而衣着华丽的她先是一惊，然後面色複杂地握紧自己小小的拳头。

看着这样的蛭涟，贺由什麼也没说，只是静静地等候着。

两人间的气氛就像凝固了一样，时间仿佛都为之静止。

“如果我说是喜欢的话呢……”

虽然声音很小，但是却已经足够用来打破这份沉寂了。

“那样的话请恕我拒绝。”

“可……可以让我问一下原因吗？”

尽管强行让自己装出镇静的表情，但是抽泣的声音还是控制不住地从声带裡发了出来。

“接下来说的话请千万不要太往深里想，不过就是一个普通高中男生的无聊的自我陈述罢了……”

“…………”

“我呢，小时候开始就有一个青梅竹马，她人很可爱，聪明又会察言观色，对谁都很温柔……简直就是八面玲珑，一开始我非常讨厌她。”

“…………”

“但是呢，久而久之发现，就连自己的都被她的那份魅力吸引住了。难以接受，我输了，无论身心都是，这份单向的恋情随着时间和年龄的增长变得越来越深，到了不能自拔的地步。”

“……贺由君说的是芥檩同学对吧？”

“没错，而且我打算今天向她表白……”

“那我真的是做了些很过分地事情呢，但是确实像我这样的女孩，完全配不上……”

“没有那种事！！”

“诶？”

贺由强硬地打断了，他的脸上带着的简直是愤怒一般的表情。

“蛭涟同学才是……十分优秀的女孩，我在高一的时候就明白了。

明明自己不擅长和别人打交道，为了搭上话却向男生借轻小说来读……

并不是随便说说而已，自己还认真地读完了，很厲害啊！就连我自己都没读完那本书啊！”

“…………”

“因此我才辞去了图书委员的，大概是潜意识裡害怕如果和蛭涟同学接触更多的话，自己对小檩的恋心会改变……蛭涟同学就是这样充满了魅力的女孩子！！”

幾乎是叫喊着，将这段话说了出来，不仅仅是主动说来让对方死心那麼简单的事情，贺由将自己真正的想法告诉了眼前的少女。

“贺由君真是……狡猾。”

“嗯，我承认这一点。”

“连告白的机会都不给……”

“因为我害怕会一时心动改变主意。”

“就那样讨厌我吗！！”

感情已经有些决堤了，难以想象自己的语气会变得这样衝动。

“不，就是因为除了小檩之外还同时喜欢着你所以才这麼说的啊！！”

“噗……”

蛭涟扑哧一下笑了出来。

“被喜欢自己的男孩子主动拒绝……无论哪种神话裡都不存在这种事呢。”

“抱歉了……”

“不，贺由君根本就不需要道歉，这原本……就是我的单恋罢了，其实今天也许根本就没有胆量说出来的……够了，就请让我稍微安静一会儿吧。”

“那麼，明天再见了……”

说完，贺由就转身离开了。

“完败了，不仅被芥檩，现在还被贺由君同情……呜呜，诅……诅咒你们喔。”

牵着公主装的裙子，少女抱着自己的膝盖坐在椅子上抽泣着。

12点的钟声看来已经响起了，辛杜瑞拉依然穿着华丽的服装，尽管王子并没有追上来。

…………

【放弃一切希望吧，进入这裡的你。】