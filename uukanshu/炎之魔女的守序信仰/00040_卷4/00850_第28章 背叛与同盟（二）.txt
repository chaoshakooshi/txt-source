“奥菲莉亚，妾身一直有一个疑问……”

“嗯？”

“男爵真的是我们脑中想象的那种无恶不做的人吗？”

听到我这样提问，她皱了皱眉。

“不知道……对我而言是必须要杀的人，仅此而已，克莉斯……不会到这个时候改变主意吧。”

毕竟是自身就是因为男爵而落到这个地步的被害人，她的事情并不是我可以插嘴的。

根據现在的情报来看客观来说他是一个慾望很重的人，除此之外的事情还是需要见到本人才能判断。

“当然不会，但是妾身并不是杀手，妾身要知道自己的目标是什麼样的人，然後才能下手。但是奥菲莉亚要动手的话无论他是什麼样的人妾身都不会阻拦的。”

“嗯，谢谢你，这样就够了。”

“不过呢，妾身真正在个人感情上更讨厌的是黑妖精这边哦。”

“是吗？但是其实他们除了正常的非法交易外还算是很守道义的，貌似抢劫也只会盯着贵族和有钱人，贩卖的女孩子也基本都是贵族女兒。虽然这样做也有问题就是了，但是他们也有自己的原则。”

“这类人往往在平民间还异常的有人气呢。”

“没错……黑妖精的组织内大多为亚人，但是也有人类存在，很多都是仰慕他们行为的一些少年。”

所以我刚才击倒的那个少年就是那种情况吧。

不过奥菲莉亚一直以为看管钥匙的是一个强壮的守卫，看来他们并没有对她给与充分的信任。估计现在倒戈也是意料之中吧……

“奥菲莉亚对於黑妖精的事情知道多少呢？他们好像会用一种特别的方式可以在禁魔区使用魔法。”

“具体的我也不是很清楚……游隼之前只是和我说如果利贝尔被放到就让我杀了你，至于他们为什麼能用魔法，哥哥知道的要更清楚一些……他从黑妖精手中得到的就是那种力量。”

游隼，这个名字再次从她的口中听到了，大概就是黑妖精们的首领没错了。比起名字来说倒是更像是外号。

嘛，问多了也只会让奥菲莉亚烦恼吧。

现在我手上持有“龙王的逆鳞”所以即使是变身这种高级的咒语也是用不出来。

之前虽说要好好揍那个什麼游隼一顿，可能实行起来有些困难。

对了，莲貌似也在查找我这边，先说明一下我这裡的情况。

【莲，在线吗？】

稍等片刻之後，头脑裡回响起青年的嗓音。

【克莉斯！？现在的情况怎麼样了？】

【呵呵~~稍微有点複杂，现在还增加了一位同伴，总之，一下说明非常困难。对了，之前有件事忘记说了，妾身的眼睛突然能看见了哦，虽然不知道原因就是了。】

【什麼？！能看见了吗？】

另一道少女的声音插入了我们的对话，对了，赫卡忒一直和我们保持着交流的。

【恩，妾身自从进入有A级封魔效果的牢房之後就变得能看见了。】

【难道是！“龙王的逆鳞”！！】

【诶？！赫卡特也知道吗？这种东西。】

【这已经不是知不知道的程度了，这可是先祖被光之勇者罗德切下的逆鳞所做成的魔道具哦。】

【唔，难道说妾身的眼睛和这个有关係吗？】

【这要看了实物才知道，总之把它好好带在身上吧，虽然可能受禁魔效果影响，但是没有的话你可能就会再次失明了。先不多说了，这边也有很多情况，根據你之前给的地址人家已经到了这裡了，男爵的秘密洋馆……】

【恩，请加油吧。】

【当然的吧~~莲，人家会非常努力的哦，所以回来的时候请好好奖励人家！】

【拜托你了……】

【包在人家身上哦！】

说完赫卡忒就关闭了聊天。

但是既没有肯定也没有否定，只是希望让她好好完成任务……还真是残酷呐，莲君。

赫卡特，单纯地扔直球也是不行的啊，像莲这种类型的还是要傲一下娇一下才更有用吧。

【妾身现在打算在这裡再呆一会，请不必担心~~】

【……又有什麼新的想法了？】

【啊哈哈~~因为有一个很想揍的人。】

【你现在在哪裡？】

【诶？这个……等一下，妾身问一下那位同伴吧。】

【不用了……调成外接模式让我自己来听吧，90度逆时针旋转耳坠就好了。】

这小东西的功能还真不少呢。

我立刻按照他指示的旋转了一下耳坠。

【恩~~可以了吗？】

【好的，周围滴水的声音都听得很清楚呢。】

唔，在某种意义上是很危险的东西呢。

“啊，克莉斯的耳坠很漂亮呢~~”

看到我的小动作，奥菲莉亚小姐也跟着我的手指看了一眼我的耳坠。

“是这样吗？其实是别人送的。”

“诶？脸都红了呢，难道是……男孩子？”

“嘛啊……算是啦~~”

“是喜欢的男孩子送的？”

“不，不是那样啦！”

唔，但是又不能当着他的面彻底否定……那样会让男生受伤的。

“立刻就又红了一些~~真好懂~~”

“……”

好羞耻，一想到莲这傢伙也在听立刻感觉羞耻度变高了一倍。

【……】

有种立刻就想把外接关掉的衝动，不过为了大局还是忍忍好了。

少女掩盖羞赧的神技其之一—转换话题。

“话说这裡还真是大呢……明明是在地下。”

“倒不如说正是因为是地下所以才能做得这麼大，这个城市的下水道据说很久远的过去时原本全是地牢，关押我们的那间也是现成的。黑妖精们只是在这个基础上改建了。”

“奥菲莉亚知道现在我们身处的位置吗？”

先让莲知道一些有用的情报吧。

“现在我们是在东北部哦，这附近就有可以出去的下水道口。”

也就是说在在我们原本的旅店附近了。

但是都到了这个地步，还是要先出去吗？

【克莉斯，先回来……】

【不要……】

【别这麼固执，现在的你无法使用魔法吧。】

【没错，但是如果出去之後可能就没有机会了，黑妖精们不是傻瓜。】

【别任性了！你连对方的实力都不知道，难道想自投罗网吗？没有什麼比你自己的生命要更重要！】

莲少见的带着怒气。

谢谢，果然一直带着那种让人觉得很可靠的气息呢。

【也许是这样呢，不过那个时候交给你就可以了吧~~哥哥~?】

【啊啊，我明白了，护卫这麼麻烦的公主大人，也是我自找的。】

【嘿嘿~~如果平安无事地回来就奖励哥哥一下。】

【唔……你，你别胡说些……】

看来是动摇的不得了呢，萝莉控哥哥。

不过呢，好像给自己立FLAG了。

“克莉斯，前面有守卫过来了！”

“啊。”

我急忙回过神，奥菲莉亚抓住我的手躲到了木箱的阴影处。

两人在离我们很近的地方停住了。

“你不觉得周围有奇怪的气息？”

“你这麼一说倒是……”

即使是半妖精，他们的五感还是要比人类要敏锐得多。

脚步声慢慢在靠近。

（左边那个交给你了，我数到3同时攻击好吗？）

奥菲莉亚凑到我的耳边用着非常轻的声音说着作战计划。

好痒……耳朵莫非也是我的弱点？

不过他们的听觉很好，不用这麼低的声音很可能被听见，。

下次让莲也给她一个耳坠好了。

不然这种关键时刻叫出声就……

唔，我的脑子最近怎麼总往那方面转啊，现在不能想这些。

我拍了下自己的脸颊，看着奥菲莉亚伸出她三根纤细白皙的手指。

（3……）

（2……）

（1……）

轻轻点头示意之後，我们一起冲出了箱子。

我直接飞起一脚踢向左边的守卫。

“唔？！”

被挡住了？

意想不到的情况让我转向奥菲莉亚那边。

只见她愣在原地。

怎麼了？为什麼不攻击？

片刻之後，她大声叫了出来。

“糟糕了！！是游隼和伊欧！快逃！！”

不会吧……

这时候我才仔细看清面前对手的脸。

【發生什麼事了！克莉斯。】

估计听到声音也能判断形势不对了，莲急切地问到。

很抱歉，现在没有集中精神回復的餘裕了。

“本来确实是为了去找的，但没想到这麼快就送上门了。还以为是什麼老鼠，原来是你们两个，呼呼。”

我的眼前伊欧早已一甩在赌场的那副绅士笑容。

我反射般地向後跳跃了。

原本站着的地方有木质的尖刺戳穿了地面。

“…………”

冷汗沾湿了衬衫。

果然这群傢伙能用魔法，即使我身上带着“龙王的逆鳞”。

究竟是怎麼做到的，还是说有什麼特殊的道具。

“奥菲莉亚，你果然还是背叛了呢……你和你哥哥都不能信任，你那废物哥哥什麼任务都做不好。”

那个人大概就是游隼吧，他留着比我要暗淡得多的银色长髮，看上去不过20多岁。

不过妖精是不能从外表判断年龄的。

“不准你说哥哥的坏话！！”

被挑衅弄得头脑血气上涌的奥菲莉亚忘了之前的逃跑指令，举起拳头就冲了过去。

以少女来说是很凌厉的一击，但是轻鬆地被游隼接住了。

然後他顺势抓住奥菲莉亚的拳头，回转朝着她的腹部一掌，奥菲莉亚便向着我这边飞了过来。

“唔……”

咚！

结果是两人一起撞到了墙上。

好疼。

【克莉斯！！嘁，你们等着我！！】

要是很快能来他估计也不会这麼着急了吧。

但是没想到不仅能用魔法，就连体术也是对方强吗……

“不用魔法呢，还是说用不出来？魔女小姐，看来把龙王的逆鳞让你带在身上是正确的呢，如何？我的见面礼。”

我勉强站了起来，重新摆好架势。

“谢谢贵重的礼物，不过妾身是不会还给你的。”

“那还真是有点伤脑筋呢，其实我并不讨厌魔女，和我合作怎麼样？我们应该没有利益的衝突。”

那还真是抱歉，可不是什麼都能用利益来衡量的呢。

我现在只是想揍你而已。

而且在见了你本人後发现自己并没有想错。

“妾身拒绝！”

“嘛~~我猜到也是这样，那麼就开始吧……放心吧，我没有虐待倾向，痛苦只有一瞬间而已。”

他的脸上留下的只有冰冷的杀意。