她在门口静候了很久，即是事态紧急也不可随意进出这个房间。

“你疯掉了吗？公主现在只有8岁而已！”

这声巨大的咆哮更是让她心头一惊。

原本抬起的右手也不自觉地落下了。

到底该不该敲门呢，各位大人的讨论貌似十分激烈。

“你这块老木头的思维何时才能改变一下呢？布雷尔。”

被称为布雷尔的男子露出了厌恶的表情。

布雷尔·古斯塔夫是希尔芙利亚王国的骑士，同时也是资历最老的贵族之一。

勇猛，正直，忠诚。

身为保守派领导者的他

最讨厌被人叫做“老木头”了。

尤其是对手还是他最讨厌的政治对手的时候。

“伊弗尔德王国是我们最大的领邦，没有任何理由拒绝。”

“现在还是在和平年间，奥斯汀。而且现在和亚历克斯王执政的情况已经不同了，年幼的二世国王已经完全被鹰派操纵了，他们看中的只有公主的潜在力量而已。”

周围同意古斯塔夫的贵族们也不禁默默点头。

看起来这是一桩政治联姻啊。虽然说国王陛下经常会收到其他国家王室的联姻请求。

一般来说，国王陛下都会以公主年龄太小而拒绝。

拥有的祝福之力的王室血脉不可能轻易送给那些小国。

但是如果对象是伊弗尔德王国的话大臣间自然不免一番口水大战。

而现在面对直言戳破事实的布雷尔，就连一向言辞尖锐的奥斯汀·巴纳德也只能摸着下巴

思考着最合适的反击言辞。

听了臣下们的讨论後，一直沉默的他开口了。

“不必多言了，巴纳德卿。确实那个小鬼和亚历克斯王不同，我们没必要这麼顾及他的面子。”

“可是，陛下，这样做是相当危险的。正是因为是鹰派掌权我们才需要多考虑一下。”

听了忠诚的臣子的话语，坐在最上位的他仅仅露出微笑。

“我不曾怀疑你的忠诚，巴纳德卿。”

“天下无双的贸易之国不会成为他们的目标。”

“这次就先拒绝吧……而且”

叩叩叩~~

看准时机的她终於抬手敲响了会议厅的大门。

“进来吧。”

等候已久的女仆轻轻走了进来。

虽然话被打断让国王很不愉快，但是这种时间敢敲门打扰一定是紧急情况吧。

“陛下，大事不妙，各处都找不到小公主的身影。”

果然又是吗……国王无奈地摇了摇头。

“凯瑟琳，我不记得自己什麼时候变成保姆了。这种事情就交给主管就好，没看到我们还有很重要的事情要谈吗？”

小公主并不是第一次失踪，应该说自从懂事以来那孩子就不停地到处乱跑。

可以说好动地简直想让人带她去看医生。

这种小事，根本不该通知给国家的管理者。

“去图书馆找找吧，不是每次都在那吗？”

“非常抱歉陛下，6个小时前我们就开始不断寻找了。幾乎已经把王宫翻了个底朝天，如果不是紧急情况绝不敢惊动陛下，但是据侍卫队长的推测，公主很……很可能出城了。”

什麼？那还真是非比寻常啊。有一点麻烦了

“凯瑟琳，通知我的护卫队，换上便服。在城下町仔细寻找，注意不要走漏消息。”

“是的，陛下。”

女仆深深鞠了一躬，退下了。

“…………”

“…………”

刚才还吵吵嚷嚷的大臣们现在都沉默着。

对着国王投来了同情般的视线……

“果然还是太早了，抱歉啊，布雷尔。”

“呵呵，这还是你第一次向我道歉。”

“唉。”

至高无上的他再次叹了一口气。

………………

…………

在城下町的小巷裡

（不论如何，这次不能轻易被抓住。）

机会只有一次。

下次肯定不能再这麼轻易出来了。

黄色的头带束着及腰的长髮。

樸素的农家女孩连衣裙。

背着破破烂烂的布制挎包。

用魔法改变的银髮红瞳已经都被染上黑夜般的黑色。

果然我还是忘不了这个颜色呢。

必须快点行动了。

我喝下一瓶药剂。

“咕噜，咕噜。”

感觉精神再次充沛了起来。

先是变出自己分身的幻术。

接下来施放了改变发色和服装的幻术。

最後是弄晕守卫的攻击魔术。

再怎麼有王室血统的加成，精神力也快要见底了吧。

所以在小巷中补充好精神力再行动才是正确的做法。

但是不可以耽误太多时间。

老爹的亲卫队看来也应该行动了。

目标只有一个，希尔芙利亚城下町的最深地牢。

我要获取那裡被封印的魔物。

——炎狱恶魔

虽然以前的我是个路痴。

但是现在完全不同了。

我看着那个与周围的民居格格不入的高大建筑物。

这麼明显鬼才看不见……

入口处有一个全副武装的骑士如同仁王般守着地牢。

我看向了骑士的肩章。

（格里芬团的纹章。有点麻烦啊。）

嘛啊，试试常规办法吧。实在不行再用魔法

不，最好不要使用，毕竟是最精锐部队。

以一敌百的王国精英。

强闯的话目前以我的水平来说很危险。

况且他们那华丽的铠甲可以反射大部分魔法。

我径直走过去。

带着微笑，盯着骑士。

是个带着阴沉表情的年轻人。

看到我之後他也低下头来。

“有什麼事吗？这裡可不是你这样可爱的小姑娘来的地方哦。”

表情还是那样阴沉

我收起笑脸，用手背巧妙地抹向眼睛。

“呜呜呜呜……哇哇”

大哭起来。

“唉？！發生什麼事了？你别哭啊”

被我的哭声所吸引，大街上的人们也向骑士投来了指责的视线。

“喂！我什麼都没做啊。”

骑士表现得十分紧张，向周围大喊着。

我轻轻拉了一下他的护腿。

该死，虽然我对这副身体再满意不过。

但是只有130公分左右的身高有时候还是非常不方便。

在图书馆时也是经常无法够到书籍。

更何况这世界不存在飞行魔法这麼方便的东西。

“呜……大哥哥，我的爸爸被当兵的抓进去了。呜……他不过是偷了一个麵包而已，这是生病的妈妈打了一个月工挣得工钱，请让我见见他。呜……”

我从包裡掏出几枚银币，同时用左手擦着“眼泪”。

“呃……”

骑士愣了一下，然後露出了明显动摇的表情。

“抱歉，小姑娘……我很同情你的遭遇，但是我必须坚守我的职责，我相信你的爸爸不久就可以回家了。”

“呜，呜……爸爸，呜哇哇哇”

貌似真的想起以前去世的老爹了。

我的眼角再次湿润了起来。

豆大的泪珠啪嗒啪嗒落在地上。

“呜呜……爸爸得罪了一个富商，他一定会被害死的。呜呜”

“唉？放心不会啦，最近入狱的盗贼没有用一个会被判死的。”

“呜哇哇哇”

随着我闹得越凶，围观的人们也越来越多。

甚至有人开始无视阶级开始大叫起来。

“什麼啊？这就是维护王国的高贵骑士？”

“净做一些下三滥的事情。”

“这孩子实在是太可怜了。”

“没人性！！”

“““让她进去！让她进去！让她进去！”””

骑士脸上布满了汗珠。

“知道了！知道了！大家安静，我会让她进去的。卫兵！带她去！”

比预想的要费力气啊。

幸好没有直接用贿赂而加了点演技。

不愧是最精锐部队。

为了荣耀而活的高贵笨蛋们。

这麼正直的傢伙也是少见啊。

“这裡就是全部的盗贼了。小姑娘？你的父亲是哪个？”

带路的卫兵向我询问道。

地下一层，离我的目标还很远。

但是今天王宫裡有重要的会议，地牢的守备力异常薄弱。

我就是挑准了这个时间才来的。

“抱歉？不过我想他应该不在这裡。”

“唉？这裡就是全部了啊，底下都是些危险的犯人了哦”

“诺姆精灵，带来睡雾之安眠！”

“哎……你是……”

噗通，卫兵倒地。

牢裡的犯人们都用惊愕的目光在我和倒下的卫兵间来回扫视。

“唉嘿嘿……”

轻轻吐出舌头，克莉斯公主殿下的招牌微笑~~

一个父亲早就不在了，还有一个正在王座上呢。

得抓紧时间了，被骑士发现也只是时间问题。

整个地牢一共有五层，这是之前逃出来之前就已经调查好的情报。

这裡的环境就像过去玩过的RPG遊戏裡一样

每隔五米左右的两边石墙上被安置着火炬，但是依然感觉很昏暗。

钢铁的牢笼更是让人不敢往裡看。

越往下走这种心情便越是强烈。

地下二层是山贼强盗以及谋杀者。

地下三层是亚人种，哥布林，兽人占了大多数，偶尔也能发现黑暗精灵的血红双瞳。

到了四层时就只剩下低吼和恶心的恶臭……

从这裡开始已经不是“人形生物”了

感受到周围怪物们的视线，一秒也不想在这裡多呆。

再往下走是一段比上面要长幾倍回转石阶。

脸颊上不禁渗出汗水。

我很清楚接下来要面对的生物有多可怕。

幾乎翻遍了皇家图书馆的所有恶魔研究者的著作，但涉及到这种生物时一向喋喋不休的学者们只会用寥寥一句话带过。

【普通人绝对不可靠近的危险存在。】

我不禁思考。

真的是可怕到这种程度吗？

是因为力量的可怕？

不，更大的可能来源于恶魔们那些未知的谜团。

他们隐藏了什麼。

某些值得部分人抛出性命也要获取的东西。

而现在，我就是这其中的一人。

终於，到了这裡了。

不出所料，巨大的石门耸立在眼前，这绝非用用下级魔法就能打开的东西。

但是，我早有準备。

从挎包裡取出有着风精灵形象装饰项链。

“以风精灵的契约者，希尔芙利亚的王族之名，许诺高贵的妾身通过此门！”

晃动，整个地牢仿佛都在摇晃。

太好了，看上去是有效的。

石门徐徐打开了。

一切都在我的预想之内，接下来就是不可预测的情况了。

下定决心的我向前迈出了一步。

没有恶臭，没有低吼，整个方形如同祭坛上的他彰显着自己强大的存在感。

我无法停止自己纤弱双腿的颤抖。

我尽力不让脸部痉挛保持住自己的微笑，以表示自己毫无敌意。

高达3米的巨大躯体被十多条紫色的魔法锁链紧紧缠绕着。

镶着红宝石的巨大战靴下的複杂封印阵散发着微光。

即使这样也绝不能认为自己完全安全。

因为他已经将目光投到我的身上。

那是好奇，仿佛看见了新玩具的视线。

接下来，不能因为恐惧而逃跑。

耍些小聪明或者使用魔法也是没用的。

那傢伙是犯规的存在。

强大而残忍。

只有遵从内心，说出自己的愿望，用上準备了一个礼拜的话术。再加上一点点运气获得他的认可才有可能成功。

我等待着他先开口，否则就不能理解这个怪物的想法。

盯着我许久的恶魔在仔细观察我之後，说出了第一句话。

【看来是没什麼威胁的客人呢。】

那是仿佛从地狱深处传来般的低沉而又嘶哑的男声。

咕噜……

我不禁咽下一口口水。

【那麼，你想要做什麼？该不会是迷路了吧。】

戏谑的语气表达出了他对我的不屑。

我用尽力气让自己不再颤抖，从口中吐出坚定的话语

“不，炎魔大人，妾身是来此释放您的。”

恶魔愣了一下，然後咧开嘴笑了出来。

【哼，才百年不出门，看来人类的幽默功力又变强了。】

难道没察觉到吗？

我为什麼敢和他这麼说话。

我低头看了看自己。

一副农家的普通女孩的打扮。

原来如此，我还保持着幻术。

不过可以确认的事情有一件。

看不穿低阶的幻术，看来他的魔法被完全封印了。

我的信心稍微膨胀了一点点。

“抱歉，这真是失礼了。”

“胧，褪去！”

轻闭双眼，念起消除的咒语。

一阵光芒散去。

身上破破烂烂的连衣裙成了华丽的礼装、

黑色的秀髮也染成了与之完全相反的银色。

睁开双眼，与恶魔肤色相同的赤瞳倒映着他的影子。

【喔……】

轻声一叹。

恶魔露出了原来如此的表情。

【原来是弗莉亚的子嗣。】

弗莉亚是希尔芙利亚的建国女王，传说中的英雄。

我的银髮红瞳很可能就是受到她的显性基因影响。

希尔芙利亚的传说中，弗莉亚讨伐了出现在地上作恶的魔神。

但是那并非她一个人的力量。

有一位穿着赤红铠甲的骑士一直陪伴着她。

【不过你弄错了一点，我并不想从这裡出去。】

“是因为约定吗？”

恶魔的眼神动摇了。

张开的嘴裡能看见巨大的利齿。

【你是怎麼知道的？！】

话语权看来落到我这边了。

看来冒险一猜取得了出乎意料的效果

“取得伟业之後，一直相伴在弗莉亚身边的红衣骑士下落不明，有人说他隐退了，也有人说他在讨伐魔神的时候战死了。”

我督了一眼恶魔。

然後再次缓缓开了口，并加重了语气。

“对吧，艾利欧格大人！”

红衣骑士应该就是眼前的恶魔。

这是我推测的结论。

当年其实弗莉亚和红衣骑士没有足够的实力击败魔神。

但是擅于使用魔法的弗莉亚将骑士的身体为容器作为代价，将他和魔神合二为一了。

如果是这种程度推理，大概能猜出的人会有不少。

但是仅仅是这个国家的地牢裡存在恶魔这个情报也是用了催眠术才从国王那裡套出来的。

连母后和姐姐都绝不知道。

守卫地牢的骑士当然也不可能知道。

他应该仅仅是被告知一定要守好地牢而已。

这个最高机密的情报，只有希尔芙利亚的最高者登位时才会被先任国王告知。

为什麼我会知道这种事？

因为我幾乎对王宫的每一位有名贵族的都用了这种问话的法术！

好好运用公主的身份和美少女的可爱，并不困难。

这次，换做恶魔露出谨慎的表情了。

【你究竟是谁？我不曾见过如此狡猾的女人。】

“克莉斯是妾身的名字，希尔芙利亚王国第二王女是妾身的身份，大人。”

低下头，两手抓住镶滚边的裙子，轻轻行了一礼。

我向着眼前的恶魔骑士介绍着自己。

【那还真是年轻的身份啊，和你完全不符。】

“妾身也是这麼认为，不过时间十分有限。接下来妾身的请求还请艾利欧格大人一定要允诺。”

恶魔沉思了一会，点了一下头，示意我继续往下说。

“请赐予妾身契约，让妾身成为魔女！”

没错，这才是我真正的目的。

成为魔女，才是最方便的获取不老不死之力的方法。

从两年前学习了一些基本魔法之後，我幾乎将全部的精力投入研究如何不老不死。

重生获得了完美的身体，完美的身份，但是我害怕衰老，因为之前作为尼特而死时我已经30岁了。

要让我眼睁睁看着这样的美少女的容貌慢慢变成30岁的阿姨？

开什麼玩笑！！

教授魔法的宫廷魔法师不会被催眠迷惑，所以我便拿出一副天真的求知学生的样子。

这群老傢伙一开始都表现极为谨慎，但一旦从一些简单的问题慢慢提问，最後他们都会喋喋不休地道出我想知道的东西。

不会有人能猜到年幼公主会怀有这样的想法。

最终结论是，在不想让肉体变难看的前提下，成为魔女是最可行的方法。

「在成为魔女前是少女，有较高的魔法天赋，强烈想成为魔女的愿望」

这三个必须的条件毫无疑问我全部拥有

只要眼前的恶魔肯结下契约，就可以达到我的目的了。

为此，即使是偷走风精灵项链这样的大罪我也绝不犹豫。

【你的每一句话都让我吃惊。】

【虽然我身为艾利欧格的部分强烈地拒绝着你的提案，但是同时作为炎之魔神的我无法拒绝如此强烈的渴求！】

【我和弗莉亚的约定是控制好魔神的力量，不再让自己到地上捣乱。】

【但是和你结下契约後，我就可以百分百控制自己的力量。这样不会违反约定。】

【那麼，我同意了。用那项链解开这个束缚吧。】

哎呀……

虽然我的目的基本已经达到。

但是幾乎没有成功的实感。

简直是顺利过头了。

之前準备好的当他拒绝时所要说的台词看来也全都可以省略掉了。

难道是恶魔的骗局？

这是不可能的，恶魔也许邪恶，也许善良，也许残忍。

但是无论哪一个都不会违反契约！即使只是口头约束。

约定对他们来说，事关性命。

“是的，立刻实行。”

不再浪费时间，现在随时被发现也不奇怪。

从挎包中取出银质小刀。

轻轻在食指上一划。

我继续向前走上祭坛，将鲜红的血液滴在封印的魔法阵上。

同时高高举起风精灵项链。

吟唱起同样从父王那打听来的咒语。

“妾身以风精灵的契约者，精灵魔法的代行者，希尔芙利亚的最高贵者之名下令，解除所有束缚。”

唔……

眼前的所有魔法锁链一根根渐渐断掉，魔封阵也在一阵强烈地光芒後失去了效果。

然後整个地牢再次摇晃起来，而且比上一次更加强烈，我连站都站不稳了。

这可不得了，如果整个地牢崩塌这祸就闯大了。

“呀啊！”

终於站不住的我向身後倒去。

……

…………

唉？没有摔倒？

一双有力的手接住了我。

稍微仰起头看向这双手臂的主人。

年轻的男性，英俊的脸，不，这已经是超人气偶像的级别的容貌了。

红色长髮和绯红的铠甲。

我一般是很讨厌这种帅哥的，但不可思议的是他没有那种做做的氛围。

虽然已经猜到了他的身份，但是因为反差过大我还是不禁询问

“难道是，艾利欧格大人？”

“除了我难道还有谁？我的魔女公主殿下？”

他的整个声音都改变了，那个地狱深处的男低音居然变成如此有磁性和魅力的声音。

我为什麼要心跳加速啊！而且脸上不自觉地染上红霞。

“你还是挺可爱的嘛，刚才说你狡猾实在有点不好意思。咳咳，我更正一下，作为艾利欧格的部分也同意这个提案。”

唔……这是受身体的女性费洛蒙影响！我虽然是尼特，但绝不是GAY！

以往都是和BBS上的狂热同人女苦战到底的！

“请，请不要戏弄妾身！艾利欧格大人！”

糟糕……咬到舌头了。

“呼呼……”

艾利欧格狡黠一笑。

这傢伙绝对是在逗老子！

“看来时间不够了，而且在没缔结契约的情况下这个形态不能保持很久，快完成仪式吧。”

唉？虽然地牢晃动的十分剧烈。

但是很快就恢復了平静，他指的时间是指？

哒哒哒……

逐渐逼近的奔跑声才让我明白是怎麼一回事。

格里芬团的骑士终於发觉了。

“艾利欧格大人！请问如何完成仪式？”

不能在此功亏一篑。

只要获取不老不死的魔女之力。

即使被父王关进这座地牢也没有关係。

“交给我就好了，闭上眼睛，忍着点，不要拒绝任何东西好吗？”

“好的，请你快一点艾利欧格大人！”

说完我立刻闭上了眼睛。

啾~~

唉，这是？

我猛地睁开眼睛。

“艾利喔噶……大……”

这傢伙居然吸吮着我的嘴唇。

唔唔……连舌头都伸进来了。

不要啊，第一次接吻的对象为什麼是男人啊！还是舌吻！

不，好烫！

烫！

不对，不是什麼下流的比喻。是真的很烫啊，舌头就像着了火一样！

要死啦！我不要被烧死这麼难看的死法啊！

请快停下来吧，艾利欧格大人。

我刚想这麼叫的时候，发现已经不烫了。

艾利欧格大人也离开了我的脸。

我吐出舌头。

拼命将视线向下移。

虽然看不清楚，但是闪着的光芒让我意识到舌头上应该是被烙上了纹章。

一小会之後，光芒渐渐暗淡下来。

这就是契约吗？我还以为是更正式点的方式。

有个什麼契约书之类的。

真是简单暴力啊。

话说回来，我还被艾利欧格大人以公主抱的方式抱在怀裡。

“已，已经可以了。艾利欧格大人快放妾身下来。”

“别着急，初战就由我来引导你吧。”

“唉？”

歪过头看向门口。

银白铠甲的骑士已经站在那裡了。

看到我他难掩一脸的惊讶。

“你难道是之前的……”

一下认不出也难怪，毕竟之前我用幻术变成了农家少女来着。

现在却是穿着华丽的银髮红瞳的少女，而且被红衣骑士以公主抱的姿势抱在怀裡。

“嗨~~又见面了，骑士哥哥。”

“啊啊……我太疏忽了，真不该放你进来。不过这裡居然什麼都没有，本来还以为关着有什麼可怕的怪物。”

确实有哦，就在幾分钟之前还能看到哦。

“话说你是谁？逃狱的佣兵？门口的卫兵也是你干的吗？”

骑士将视线转向艾利欧格。

艾利欧格完全无视骑士的问话。

抬头看了看天花板，然後继续向我低声说道。

“又有更多的人来这裡了，你觉得该怎麼办？”

“唉？”

“大概有三人，从脚步来看轻装，抱有某种目的性的可能较大。”

“这样啊，那就有点麻烦了。”

看来王宫有所行动了。

速度还真是快。

本来如果是只有那骑士一人完全可以击倒他然後使用暗示修改他的记忆。

但是王宫的人来了就不能用这麼粗犷的方法，事态如果变得不可收拾就必须逃出这个国家了。

现在稍微减轻一下自己的罪名吧。

“艾利欧格大人，请问你平时可以隐去身形吗？”

“我可以寄宿在你身上，但是不应战吗？”

看来这位大人并没有想象的那麼聪明啊，或者是因为自身太强了直接无视世间的法则吧。

“艾利欧格大人，以妾身的立场来说不能在此杀掉他们。请您稍微和他战鬥一下，然後佯装被击败然後消去身形吧。”

“哼，原来如此，虽然很不爽但这确实是现在最妥当的方法。”

“喂！你们嘀嘀咕咕说些什麼呢？我懂了！你这红衣混蛋要挟了那个小姑娘对吧。是你强迫她化妆成黑髮然後释放你的吧。”

我和艾利欧格对视了一下，露出不可思议的表情。

然後转向骑士投去同情的目光。

“笨蛋呢。”

艾利欧格大人已经忍耐不住笑了出来。

“哈哈……没错，不过她已经没有利用价值了，送你了。”

“呀啊！”

我被艾利欧格抛了出去

“唔……”

然後被骑士稳稳接住了。

同样还是公主抱的姿势。

今天是这个国家的“公主抱日”吗？

“喂，快放妾身下来啦。”

“看招！”

在我向骑士抱怨的时候，艾利欧格已经抽出了长剑向骑士突刺过来。

骑士侧过身躯，轻鬆避过。

“喂，很危险啊。”

艾利欧格仍然只有狡黠一笑。

“快逃跑吧，小姑娘。接下来就是骑士的职责了。”

“嗯，大哥哥保重哦，别被打死了哦。”

我飞速冲出房间跑上石阶

正好看到从上层过来的亲卫队。

“公……公主殿下！”

毕竟常年身在王宫，我也立刻就被他们认了出来。

侍卫们左手扶右胸，弯下腰行了一礼。

“真没想到公主殿下会在这裡，真是太好了，陛下也相当担心您的情况。”

“不必多礼，安东尼。骑士团的大哥哥正在和绑架我的犯人战鬥呢！请快去帮帮他。”

我向其中一个叫做安东尼的侍卫说出了谎报的情况。

“这样吗？请公主殿下放心，我等一定誓死保护殿下的安全。”

啊啊，这个国家的人都好骗过头了。

现在艾利欧格和骑士应该正打得激烈呢。

希望他不要失控，快点佯装失败吧。

我跟着那三人，从大门後看着战况。

“唔……”

骑士使用着两把巨剑，以极其迅速的手法交换着不停斩击。

“喂，一直防御着可没有机会啊。”

骑士对着眼前的对手做出了挑衅。

无视了挑衅的艾利欧格虽然只有一把剑，却同时使用着火焰魔法配合着对方的进攻节奏进行防御。

仅仅是看着那不合理的敏捷动作，就连外行人也能明白。

骑士很强，非常的强。

到现在为止一直占着上风，虽然艾利欧格大人绝对放了水。

但是挥舞着比我还高的两把大剑居然还能连续数分钟地持续进攻感觉不出一丝疲惫。

甚至连那身重甲在他敏捷的动作下也仿佛失去了重量。

他究竟有着多麼强的体力啊。

侍卫们根本无法插手，愣在一边看着这场完美的决鬥。

但是，我发现艾利欧格使用魔法的频率逐渐上升了。

一抹淡淡的微笑充分地显示了自己的餘裕。

“你这傢伙！留手了吗？用真本事攻过来啊！”

“是吗……本来觉得这麼强杀掉也太可惜了，但是你一定想死的话我就把你烧成灰烬！！”

让长剑上缠满火焰，向地面猛劈。

“喝！”

三道高及天花板的火柱以肉眼难以观测到宛若疾风般的速度向骑士袭来。

“！！”

这种情况下根本就无法躲开。

火焰比他的挥砍要更快！

骑士情急之下丢弃了右手的大剑。

以右肩为支点向旁边受身翻滚。

勉勉强强躲过了火柱，但是左腿却被点着了。

“唔！”

骑士脸上流露出痛苦的表情。

他迅速解开了腿铠。

然後重新站了起来。

“真能幹呢，你这傢伙的实力可以匹敌巨人了。”

艾利欧格大人平静地赞许了对方的实力。

他在笑，同时再次握紧了剑。

糟糕，这个笨蛋！

必须出手了。

我用力推开挡在身前的侍卫，跑到了骑士身前张开双手。

“公主殿下！危险啊！”

没用的侍卫们虽然大喊大叫的，却依然站在原地。

“唉？公主殿下？”

骑士一脸疑惑的表情盯着我。

看什麼看！你也是笨蛋！

“不许你再伤害无辜的人了。”

我背对着骑士和侍卫们。

眨眼向艾利欧格大人示意快住手。

“哼……”

看起来很不爽，但是理解了。

艾利欧格抬起了右腕摆起突刺的架势。

“躲在小女孩的背後也敢自称为骑士吗？”

“你说什麼？！”

“要是个真正的战士的话就接下我这一击吧。”

喂？你想幹什麼。

我向艾利欧格大人皱起眉头的时候，他轻轻笑了一下。

是不必担心的意思吗？

好吧，那就交给你了。

“请让开吧，公主殿下，我卡尔作为骑士团的一员有保护老弱妇孺的义务。”

可以不要说这麼羞耻的台词吗？你以为我想啊？

那就交给你们啰。

我放下双手，退到後方。

同样，自称卡尔的骑士也架好了巨剑。

空气在此刻也凝固了。

双方眼神交汇之後，同时踏出脚步。

“哈！”

“喝！”

铛——

决鬥的结果是，卡尔以毫厘之差躲过了刺向了他脖颈的长剑。

但是艾利欧格却被大剑整个贯穿了。

“唔……真有你的啊。”

喂喂，棒读了哦。你给我认真点演哦！

“哼，邪不胜正。”

卡尔话音未落，艾利欧格的身体已经化作尘土

赤色的铠甲也哐当一声落到地板上。

“原来是魔物，难怪有此等力量。”

卡尔说完捡起大剑，将其收入剑鞘中。

“你没事吧，公主大人！”

马后炮的侍卫们看到危机解除也跑了过来。

“我没关係哦，你们应该关心的是那位骑士哥哥吧。”

“我没有关係，之前失礼了公主殿下。”

我看向卡尔被点着的左腿，非常严重的灼烧伤，就像烙铁烙过的痕迹一般印在他的小腿上。

“这麼严重的伤还说什麼没关係，等一下哦。”

我弯下身去，双手重叠对准骑士的伤处。

“至高的光之神，请允许妾身为消除带给人们苦难的罪恶而借用您的神力。”

治癒魔法很特殊，与其说是咒语不如说是向神明祈祷，非常难以掌握，我虽然并不擅长，无法做到挽救死者接回四肢。但是如果仅仅是治疗灼伤这种程度还是不成问题的。

“这样就应该没问题了……唉”

伤口并没有癒合，不……没有圣光，法术根本没有成功。

唔，头有点晕晕的感觉。

这是为什麼？

【对了，因为契约的关係你以後不能使用火属性之外的法术了。】

耳边传来艾利欧格大人的声音（地狱版），估计已经附到我身上了。

但是他说的话让我十分在意。

（什麼？变成魔女反而变弱了？）

为了不让骑士听见，我掩着嘴巴轻轻问道。

【怎麼可能，吾等的力量是绝对的，兴奋起来吧，你使用火属性魔法的威力提升了何止十倍。并且再也不需要向精灵祈求（咏唱咒语），这是你我的力量！】

（太糟糕了！你以为妾身是您那样天天喜欢找人打架的傢伙吗？常规的生活魔法对我更有用啊！）

【契约是不能取消的。】

“妾身知道！”

“怎麼了？公主殿下？”

糟糕，太大声了。

“什麼都没有哦，对不起，看来妾身还不能很好地用魔法呢，真是不好意思骑士哥哥。”

骑士露出了温柔的笑容。

“千万别这麼说公主殿下，公主殿下是王室，是女士，也是个孩子。为保护您而受伤是骑士的光荣。”

呵呵，我已经不想再吐槽了，虽然说得煞有其事我还是有点感动的。

“那麼安东尼！”

“听候吩咐，殿下。”

“让另外两个卫兵赶快带着卡尔大人去治疗，你带我回城见父王吧。对了，骑士治疗之後也请带他来遏见父王。”

“谨遵您的命令！”

好了，PLETE！接下来该想想怎麼把祸都嫁祸在这个骑士身上。