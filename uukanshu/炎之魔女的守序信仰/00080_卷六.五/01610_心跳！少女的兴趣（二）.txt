结果到了酒馆快要关门我都没有勇气再回去店裡。

哈啊，什麼啊这种背德感。

明明我什麼坏事都没做……

为什麼弄得像做了什麼见不得人的事情一样，太奇怪了。

走在回宅邸的路上，我低着头看着地面，像个做错事的孩子一样。

说到做错事的话，老爷的酒怎麼办？

他好像很期待呢，现在再折回去取吗？

但是天色已经晚了，再不回去的话少爷会担心的。

好烦呐，如果有手机的话折时候就可以通知在外面採购的梅阿莉姐顺便带回来了。

魔法终究无法做到念话之类的事情呢，还是说人们还没研究到那个地步吗？

和前世一样，一旦犯了什麼错自己总会想一些奇怪的事情。

总之，现在只能回去了。

呜，估计得挨训了……老爷虽然是个很大度的贵族，但是意外在细节上的要求还是十分严厲。而且如果他问起原因的话我该怎麼回答啊？

难道说发现了两位男性正在酒窖裡做让人害羞的事情吗？这是什麼羞耻PLAY啊！在那个狮子般的老爷面前说这种话！

好想试试看……

当然是开玩笑的。

我还不是喜欢作死的抖M女孩。

…………

……

“因为看到了漂亮的衣服忘了拿酒？这理由比梅阿莉的看见帅哥和艾露走散的事情更加荒谬……”

明明是那种事情才比较荒谬吧！

虽然很像这麼说，可是琳达严肃地盯着我的脸完全不像开玩笑的样子。似乎早已发觉我这不自然的态度藏着什麼猫腻。

回到了领主宅邸裡，看见我两手空空后，琳达吊起了眉毛眼神变得锐利起来。

“听好哦，克莉斯，女仆这个工作可不是好看的而已，我们和国王身边的骑士一样……忠于主人，达成主人的愿望才是我们该作的事情。”

说完後她转过身去将我一个人留在原地。

“下次请不要再犯了，酒明天再取，但是记得要先要好好谢罪。”

留下这句话後女仆长便离开了廊下。

唔，真是无地自容。

她是一个把女仆当做骑士道般对待的认真的人呢。

在真正的女仆面前我这样玩乐般的心态是不是有些太失礼了呢。

之後得好好道歉才行，无论是女仆长还是琳达小姐。

关於看见了不得了的事情……就忘掉吧，即使再看到伊凡大哥也要装作什麼也没有發生那样。

嗯，就这样！

晚餐时间老爷并没有说什麼，看上去琳达应该已经报告了这件事吧。

可是在那之後，我正打算帮忙收拾桌子的时候，老爷却一声不响地从背後走了过来。

“克莉斯……”

“是！是，老爷……”

突然被点名一下子让我惊慌不已，做贼心虚？这麼说似乎不太合适，但是确实让人心臟扑通扑通地乱跳，说话都像随时会嚼到舌头一般。

“最近艾文的情况怎麼样。”

“诶？啊，少爷的进步非常快呢，只花了两个星期就学完了东联盟法典和帝国史，妾身感觉渐渐也教不了呢~~诶嘿嘿。”

“是吗，这小子最近也变得懂了些规矩……这还得多谢你呢。”

“不，这是妾身的职责。”

结果说的是这类事啊，太吓人了啊，差点以为要被教训了。

虽然老爷从来没打过我，但是总是有让人恐惧般的感觉。

“如果有什麼觉得不安的事情和我说就好了，嘛，你大概认为我是一个不太好说话的人吧……”

“……绝没有那样的事情！”

“呼呼~~那就当成那样吧，不过如果想找人商量什麼的话，夫人她倒是一个不错的对象。”

“抱歉，让您费心了。”

我对着主人鞠了一躬表示感谢。

哎呀呀，其实是被关心了呢，我还真是不像样的女仆。

因为担心自己会被教训而惧怕主人，太差劲了。

果然还是有必须要做的事情。

“……老爷……对不起！”

“怎麼了？”

并没有抬起头，我依然维持着弯着腰的姿势。

“非常对不起，今天没有把酒取回来。”

“如果是这件事我已经从琳达那裡知道了，你很少会犯这样的错呢。發生了什麼吗？”

“是的……”

“不能说的事情？”

“嗯，因为涉及他人的名誉。”

“这样啊，那我就不多问了……你也早些休息吧。”

“谢谢您。”

明天必须好好完成工作把酒给拿回来才行！

回到了準备间打算换衣服洗个澡的时候意外发现梅阿莉和艾露姐也正好也在换衣服。

“今天的工作好像没好好完成呢……出什麼事了吗？”

“欸？艾露姐也知道了吗？”

“当然了，因为我们也是老爷的女仆来着，琳达报告的时候我们正好在场呢。”

“这样啊……”

“克莉斯很少会犯这样的错误呢。”

就连她们两也这样说。

其实并不代表我工作能力多麼强之类的，只是因为宅子裡的事务大多数是由管家和这三位女仆管理的，我只负责少爷的起居以及较少量的工作。

照顾少爷这件事，现在由於完全成了朋友感觉变得轻鬆了很多。

“如果不习惯出入酒馆的话明天就让我去取吧。”

解开了罩衫露出橘色的内－衣，梅阿莉姐这时候提出了助言。

“谢谢，但是不必了……毕竟这是妾身的责任呢。”

“呼呼~~别装大人哦，小妹妹~~”

她抚摸着我的头轻轻笑着。

单是这样的时候梅阿莉姐的确是一个非常有魅力的女孩呢。

很可惜我实际上都30岁了。

嗯，这边的年龄就不算了，毕竟感觉转生後心智有些变化。

“先别在意这种事啦，到底發生了什麼？”

艾露没有顾及胞姐的话，饶有兴致地看着我，再次将话题拉了回来。

本想什麼也不说糊弄过去的。

但是看她眼睛裡都快冒出星星般的样子大概是不行了。

“那个……请你们绝对不要告诉其他人好吗？”

“嗯！当然！！”

毫不犹豫地就回答了。

早知道就不说这句话了……这种绝对会告诉朋友的回答完全不可信。

“真的，真的不能告诉别人哦！会伤到他们的。”

於是我又强调了一遍。

虽然最好的方法就是不说，可是自己一人逼着秘密好难受呢，又找不到树洞给我喊。

而且和老爷说这些事有些顾虑，但是和同样是年轻女孩又比较活泼的这两人的话……

“他们？哎呀，看来大概是之类的事情被小克莉斯撞到了吧。”

艾露托着下巴，感觉对这类事很有见解来着。

“不对啦，虽然确实有些接近啦……”

“呼呼，看来果然是这类事情呢~~”

糟糕，顺嘴就说出来了。

这两人的兴趣到了沸点，放下脱了一半的衣服，就穿着凑近了过来。

唔，即使是女性看来也是太过於艳丽了一点。

“我很想知道详细的事情，可以仔细地说明一下吗~~”

唔，别怪我哦，伊凡大哥。