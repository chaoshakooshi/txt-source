无限回廊。那是在神话中经常出现的“名场景”之一。

英雄在此预知自己即将面对的命运，死者在此回顾自己曾犯下的罪行。
亦说这是神曾经降临在地上时为选定“应获得救赎之人”而建造的场所。

正所谓是“英雄”和“圣人”传说中不可或缺的插曲，因而自称“看到了自己所该前行的命运”的呆瓜也时有所见。

甚至就连王族和贵族这种了不起的傢伙之中也有说着“虽然不知道是梦还是真实存在”将其作为自己行为正当性的根據使用的人存在……不过、这种高不可及的事对艾丽莎来说倒是没什麼所谓。
不过问题就在於艾丽莎眼前这个叫要的男子，他简直就像亲眼目睹一般地描述着。
而且大多数人说辞都是“在无限回廊中上升”或“走动”，但他偏偏说是“落穴”和“落下”。
这简直说的就像……是从“无限回廊的前方”到来一样。

不管怎麼说，牛皮吹到这种地步的人艾丽莎活到现在还是第一次见。
毕竟自称是从只有神与灵魂才能进入的场所，“持有着肉体”，“从回廊的前方”到来。就算以牛皮来说，也实在是过头了。
正因如此，反而莫名地让人觉得这可能是实话。要确实困惑的样子使艾丽莎把“吹牛皮的狂人”这种捨弃不掉的想法暂时按捺在了心中。

“……那个、我没有说谎”
“嗯”
“也不是疯子……也没有说自己是被选上的人类的意思哦。不过、我说的是都是真的。而且……”
“而且？”

在那个“景象”中，你。
即将脱口而出是，要沉默了下来。
真红鳞片的巨龙。
那种话就算说出来也不一定有人会信。
而且，要更加害怕着，如果说出来的话，说不定就会被扔下不管。
为了保护自己，你即使知道赢不了也向龙挑起战鬥什麼的。
听了这种话，又有谁会愿意把要带在身边？

“而、而且……”

就算这样，不说的话也感觉有点过於卑鄙（玩弄心机）……要还是準备说出来。
必须说。可是，又不敢说。
说出来後，眼前的少女——艾丽莎会怎样看待自己呢。
要害怕得不得了。

“你、”
“等等”

就算这样，要也坚持地说出……但、这时艾丽莎遮住了要的嘴。

“大致明白了。我、發生了什麼事吧”
“……”

看着惊讶的要，艾丽莎叹了口气。
怎麼看都不像“要骗人”的表情。
虽然“看到了你即将遇到的厄运”是骗子的常用手段，但大多都会接着“我知道该如何回避它”的说辞。
但、正要以“厄运”开头的要却没有往下接话的意思。
不如说，那个反应怎麼看都像是“不知道回避方法”而迷茫的人的反应。

“要。我判断不了你说出来的话是否是真的，估计在其他地方说也会被当做诈欺师看待吧”
“呜……”
“不过、我确实看到你是从什麼都不存在的空中掉下来的”

如果没有看到那种不可思议的景象的话，艾丽莎肯定会把要所说的事当做妄想和牛皮而置之不理吧。
但，现在她看到了。
只有艾丽莎知道，无论去哪裡，被谁听到也不会去相信的要的“真实”。

“要、再问你一次……你、真的是从无限回廊的“前方”过来的吗？”
“……我不知道那个叫无限回廊的地方，而且我知道也只是在意识到之後就已经在往下掉了……”

要的说辞实在是乱七八糟分辨不清，但却有着难以置信的说服力……艾丽莎轻轻叹了口气。

“是吗。那、要今後準备怎麼办？”
“怎麼办是……”
“如果要的故乡在无限回廊的“前方”的话，我是没法把要带过去的。顺带一提，依场所不同，根據你刚才说的话，你说不定还会被逮捕哦”
“欸……”

看着被惊讶和迷惑占满的要的表情，艾丽莎真心地摆出困扰的表情。
她彻底地理解了……“放着这傢伙不管的话，绝对会破灭的”
最糟的情况，被诈欺师说些七七八八的事而被骗……这种末路简直一目了然。
虽然艾丽莎也不是那种善心和餘裕充足到会帮助每一个遇到的陌生人的好人，但姑且也是有着“救了对方”的缘分。
而且……万一要说的是实话、在这裡帮助要或许也是某种必然。

“啊——要、我这裡有一个提案”
“欸、是、是什麼呢”

双腿并在一起跪坐在地上的要的坐姿莫名得有些漂亮，艾丽莎稍稍有些无言……不过、干咳了声後继续说了下去。

“现在的要，嘛，正所谓没工作没住所的人呢”
“唔咕”

正中红心，要毫无辩驳餘地。
现在的要不仅无职且住所不定，更是可疑人物的代表範例。
而且还是个想着“在这裡被艾丽莎抛弃不管的话”这种事的软弱男。
对着因自我厌恶而低下头的要，艾丽莎“所以说”地继续说道。

“在要能某种程度自立之前，我就先帮帮你吧”
